package za.ac.wits.spaceinvaders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import za.ac.wits.spaceinvaders.yourname.Agent;
import za.ac.wits.spaceinvaders.yourname.agents.RandomAgent;
import za.ac.wits.witstestablespaceinvaders.Move;

public class Main {

    public static void main(String args[]) throws FileNotFoundException, IOException {
        File input = new File(args[0] + "/map-detailed.txt");
        File output = new File(args[0] + "/move.txt");
        try {
            long startTime = System.currentTimeMillis();
            Agent agent = new RandomAgent();

            FileInputStream in = new FileInputStream(input);
            Move m = agent.getMove(in);
            in.close();

            writeMove(m, output);
            System.out.println("Time taken for agent " + agent.getDescription() + " " + (System.currentTimeMillis() - startTime));
        } catch (Exception e) {
            writeMove(Move.NONE, output);
            e.printStackTrace();
        }
    }

    public static void writeMove(Move move, File outputFile) throws IOException {
        try (FileWriter writer = new FileWriter(outputFile)) {
            writer.write(move.toString() + System.lineSeparator());
            writer.close();
        }
    }
}
