package za.ac.wits.spaceinvaders.yourname.agents;

import java.io.InputStream;
import za.ac.wits.spaceinvaders.yourname.Agent;
import za.ac.wits.spaceinvaders.yourname.GameState;
import za.ac.wits.witstestablespaceinvaders.Move;

public class RandomAgent extends Agent {

    @Override
    public GameState getGameState(InputStream in) {
        //return null currently. This can be removed as soon as state reading is implemented in the Agent class.
        return null;
    }

    @Override
    public Move getMove(GameState state) {
        int num = (int) (Math.random() * 3);
        if (num == 0) {
            return Move.FIRE;
        } else if (num == 1) {
            return Move.LEFT;
        } else {
            return Move.RIGHT;
        }
    }

    @Override
    public String getDescription() {
        return "Random";
    }

}
