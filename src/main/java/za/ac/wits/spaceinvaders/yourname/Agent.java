package za.ac.wits.spaceinvaders.yourname;

import java.io.InputStream;
import za.ac.wits.witstestablespaceinvaders.Move;
import za.ac.wits.witstestablespaceinvaders.TestableAgent;

public abstract class Agent implements TestableAgent {

    @Override
    public GameState getGameState(InputStream in) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Move getMove(InputStream in) {
        GameState gameState = getGameState(in);
        return getMove(gameState);
    }
    
    public abstract Move getMove(GameState state);
}
