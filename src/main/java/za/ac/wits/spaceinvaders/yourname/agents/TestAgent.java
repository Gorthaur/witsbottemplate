/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.ac.wits.spaceinvaders.yourname.agents;

import za.ac.wits.spaceinvaders.yourname.Agent;
import za.ac.wits.spaceinvaders.yourname.GameState;
import za.ac.wits.witstestablespaceinvaders.Move;
import za.ac.wits.witstestablespaceinvaders.TestableAgent;

/**
 * Any agent that you want to test has to implement TestableAgent itself
 * otherwise it won't be picked up. This is true even though Agent implements
 * TestableAgent.
 *
 * These agent classes must also have an empty constructor.
 */
public class TestAgent extends Agent implements TestableAgent {

    /**
     * Empty constructor to ensure this can be tested.
     */
    public TestAgent() {
    }
    
    
    @Override
    public Move getMove(GameState state) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDescription() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
